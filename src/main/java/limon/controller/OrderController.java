package limon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import limon.service.OrderService;

@Controller
public class OrderController {

	@Autowired
	private OrderService orderService;

	@RequestMapping("/order/iphone123/2")
	public String process() {
		orderService.processOrder("iphone123", 501);

		return "redirect:/products";
	}
}
