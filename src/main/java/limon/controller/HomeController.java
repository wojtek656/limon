package limon.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import limon.service.CustomerService;

@Controller
public class HomeController {
	
	
	
	@RequestMapping("/")
	public String welcome(Model model){
		
		model.addAttribute("greeting", "Witaj w sklepie Limon!");
		model.addAttribute("tagline", "Trololo!");
		
		return "welcome";
	}

}
