package limon.service;

import java.util.List;

import limon.domain.Customer;

public interface CustomerService {
	
	List<Customer> getAllCustomers();

}
