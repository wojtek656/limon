package limon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import limon.domain.Product;
import limon.domain.ProductRepository;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private ProductRepository productRepository;
	
	public void processOrder(String productId, int count) {
		
		Product productById = productRepository.getProductById(productId);
		
		if(productById.getUnitsInStock() < count){
			throw new  IllegalArgumentException("Zbyt ma�o towaru. Obecna liczba to: " + productById.getUnitsInStock());
		}
		else
		{
			productById.setUnitsInStock((productById.getUnitsInStock() - count));
		}
		
		
	
	}
	
	

}
