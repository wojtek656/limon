package limon.service;

import java.util.List;
import limon.domain.Product;

public interface ProductService {
	
	List<Product> getAllProducts();

}
