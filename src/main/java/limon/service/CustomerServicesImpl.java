package limon.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import limon.domain.Customer;
import limon.domain.CustomerRepository;

@Service
public class CustomerServicesImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	public List<Customer> getAllCustomers() {

		return customerRepository.getAllCustomers();
	}
}
