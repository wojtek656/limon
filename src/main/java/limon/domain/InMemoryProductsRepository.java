package limon.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class InMemoryProductsRepository implements ProductRepository {

	private List<Product> listOfProducts = new ArrayList<Product>();

	public InMemoryProductsRepository() {
		Product iphone = new Product.Builder("iphone123", "iPhone 5s", new BigDecimal(700))
				.description("4 calowy smartphone od Apple").unitsInStock(500).build();

		Product htc = new Product.Builder("htc", "htc 5s", new BigDecimal(700))
				.description("4 calowy smartphone od htc").unitsInStock(500).build();

		Product microsoft = new Product.Builder("microsoft", "microsoft 5s", new BigDecimal(700))
				.description("4 calowy smartphone od microsoft").build();

		Product nokia = new Product.Builder("nokia", "nokia 5s", new BigDecimal(700))
				.description("4 calowy smartphone od microsoft").build();

		listOfProducts.add(iphone);
		listOfProducts.add(htc);
		listOfProducts.add(microsoft);
		listOfProducts.add(nokia);
	}

	public List<Product> getAllProducts() {

		return listOfProducts;

	}

	public Product getProductById(String productId) {
		Product productById = null;
		for (Product product : listOfProducts) {
			if (product != null && product.getProductId() != null && product.getProductId().equals(productId)) {
				productById = product;
				break;
			}
		}
		if (productById == null) {
			throw new IllegalArgumentException("Brak produktu o wskazanym id:" + productId);
		}
		return productById;
	}

}
