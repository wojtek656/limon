package limon.domain;

import java.util.List;

public interface CustomerRepository {
	
	List<Customer> getAllCustomers();
		
}
