package limon.domain;

public class Customer {

	public Customer(Builder builder) {

		firstName = builder.firstName;
		lastName = builder.lastName;
		streetAddress = builder.streetAddress;
		cityName = builder.cityName;
		postalCode = builder.postalCode;
	}

	private String firstName;
	private String lastName;
	private String streetAddress;
	private String cityName;
	private int postalCode;

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public String getCityName() {
		return cityName;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public static class Builder {

		private String firstName;
		private String lastName;
		private String streetAddress = "";
		private String cityName = "";
		private int postalCode = 0;

		public Builder(String firstName, String lastName) {
			this.firstName = firstName;
			this.lastName = lastName;
		}

		public Builder streetAddress(String val) {
			streetAddress = val;
			return this;
		}

		public Builder cityName(String val) {
			cityName = val;
			return this;
		}

		public Builder postalCode(int val) {
			postalCode = val;
			return this;
		}

		public Customer build() {
			return new Customer(this);
		}
	}

}
