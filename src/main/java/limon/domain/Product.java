package limon.domain;

import java.math.BigDecimal;

public class Product {

	private String productId;
	private String name;
	private BigDecimal unitPrice;
	private BigDecimal unitPriceWithVAT;
	private String description;
	private String category;
	private long unitsInStock;
	private long unitInOrder;
	private boolean discontinued;
	private String condition;
	private String manufacturer;
	private double vat = 0.22;

	public static class Builder {

		private String productId;
		private String name;
		private BigDecimal unitPrice;
		private BigDecimal unitPriceWithVAT;

		private String description = "";
		private String category = "";
		private long unitsInStock = 0;
		private String manufacturer = "";
		private double vat = 0.22;

		public Builder(String productId, String name, BigDecimal unitPrice) {

			this.productId = productId;
			this.name = name;
			this.unitPrice = unitPrice;

			unitPriceWithVAT = unitPrice.add(unitPrice.multiply(new BigDecimal(vat)));
			this.unitPriceWithVAT = unitPriceWithVAT.setScale(2, BigDecimal.ROUND_HALF_UP);
		}

		public Builder description(String val) {
			description = val;
			return this;
		}

		public Builder category(String val) {
			category = val;
			return this;
		}

		public Builder unitsInStock(long val) {
			unitsInStock = val;
			return this;
		}

		public Builder manufacturer(String val) {
			manufacturer = val;
			return this;
		}

		public Builder vat(double val) {
			vat = val;
			return this;
		}

		public Product build() {
			return new Product(this);
		}
	}

	private Product(Builder builder) {
		productId = builder.productId;
		name = builder.name;
		unitPrice = builder.unitPrice;
		unitPriceWithVAT = builder.unitPriceWithVAT;
		description = builder.description;
		category = builder.category;
		unitsInStock = builder.unitsInStock;
		manufacturer = builder.manufacturer;
		vat = builder.vat;
	}

	public String getProductId() {
		return productId;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public BigDecimal getUnitPriceWithVAT() {
		return unitPriceWithVAT;
	}

	public String getDescription() {
		return description;
	}

	public String getCategory() {
		return category;
	}

	public long getUnitsInStock() {
		return unitsInStock;
	}

	public long getUnitInOrder() {
		return unitInOrder;
	}

	public boolean isDiscontinued() {
		return discontinued;
	}

	public String getCondition() {
		return condition;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public double getVat() {
		return vat;
	}

	public void setUnitsInStock(long unitsInStock) {
		this.unitsInStock = unitsInStock;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "Produkt [productId=" + productId + ", nazwa=" + name + "]";
	}

}
