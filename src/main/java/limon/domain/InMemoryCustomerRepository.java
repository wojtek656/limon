package limon.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class InMemoryCustomerRepository implements CustomerRepository {

	private List<Customer> listOfCustomers = new ArrayList<Customer>();

	public InMemoryCustomerRepository() {
		
		Customer c1 = new Customer.Builder("Wojciech", "Kantorczyk").cityName("�ask").streetAddress("Wr�blewskiego 5")
				.postalCode(98100).build();
		
		Customer c2 = new Customer.Builder("Zbigniew", "Kowalski").cityName("Warszawa").build();
		
		listOfCustomers.add(c1);
		listOfCustomers.add(c2);

	}

	public List<Customer> getAllCustomers() {
		return listOfCustomers;

	}

}
