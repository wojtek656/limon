package limon.domain;

import java.util.List;

public interface ProductRepository {
	
	List<Product> getAllProducts();
	
	Product getProductById(String productId);

}
